-- Request statuses
create table cp_d_request_status
(
	id bigserial not null
		constraint cp_d_request_status_pk
			primary key,
	code varchar(50) not null,
	name varchar not null
);

create unique index cp_d_request_status_code_uindex
	on cp_d_request_status (code);

insert into cp_d_request_status (id, code, name) values (1, 'SENT', 'Sent');
insert into cp_d_request_status (id, code, name) values (2, 'PENDING', 'Pending');
insert into cp_d_request_status (id, code, name) values (3, 'ACCEPTED', 'Accepted');
insert into cp_d_request_status (id, code, name) values (4, 'REJECTED', 'Rejected');
