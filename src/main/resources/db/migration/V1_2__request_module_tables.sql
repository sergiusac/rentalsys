-- Request types
create table cp_d_request_type
(
	id bigserial not null
		constraint cp_d_request_type_pk
			primary key,
	code varchar(50) not null,
	name varchar not null
);

create unique index cp_d_request_type_code_uindex
	on cp_d_request_type (code);

insert into cp_d_request_type (code, name) values ('COMPANY_REGISTRATION', 'Request for registration of a company in the system');

-- Requests
create table cp_request
(
	id bigserial not null
		constraint cp_request_pk
			primary key,
	initiator_id bigint not null
		constraint cp_request_cp_user_id_fk
			references cp_user,
	request_type_id bigint not null
		constraint cp_request_cp_d_request_type_id_fk
			references cp_d_request_type (id),
	remark varchar not null,
	created_at timestamp default now() not null,
	updated_at timestamp default now() not null
);

