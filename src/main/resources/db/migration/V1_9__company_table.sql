create table cp_company
(
	id bigserial not null
		constraint cp_company_pk
			primary key,
	company_name varchar not null,
	description varchar not null,
	created_at timestamp default now() not null,
	updated_at timestamp default now() not null
);
