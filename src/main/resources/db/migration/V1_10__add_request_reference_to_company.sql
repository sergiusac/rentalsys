alter table cp_company
	add registration_request_id bigint not null;

alter table cp_company
	add constraint cp_company_cp_company_registration_request_id_fk
		foreign key (registration_request_id) references cp_company_registration_request;
