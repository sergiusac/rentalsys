alter table cp_request_journal
	add status_id bigint not null;

alter table cp_request_journal
	add constraint cp_request_journal_cp_d_request_status_id_fk
		foreign key (status_id) references cp_d_request_status;
