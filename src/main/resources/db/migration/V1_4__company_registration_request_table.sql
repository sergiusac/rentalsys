create table cp_company_registration_request
(
	id bigserial not null
		constraint cp_company_registration_request_pk
			primary key,
	company_name varchar not null,
	company_info varchar not null,
	journal_id bigint not null
		constraint cp_company_registration_request_cp_request_journal_id_fk
			references cp_request_journal,
	created_at timestamp default now() not null,
	updated_at timestamp default now() not null
);
