-- User table
create table cp_user
(
	id bigserial not null
		constraint cp_user_pk
			primary key,
	email varchar not null,
	password_hash varchar not null,
	created_at timestamp default now() not null,
	updated_at timestamp default now() not null
);

create unique index cp_user_email_uindex
	on cp_user (email);

-- Role table
create table cp_d_role
(
	id bigserial not null
		constraint cp_d_role_pk
			primary key,
	code varchar(50) not null,
	name varchar not null
);

create unique index cp_d_role_code_uindex
	on cp_d_role (code);

-- Basic roles
insert into cp_d_role (code, name) values ('ROLE_USER', 'User role');
insert into cp_d_role (code, name) values ('ROLE_ADMIN', 'Admin role');


-- Link users and roles
create table cp_r_user_role
(
	user_id bigint not null
		constraint cp_r_user_role_cp_user_id_fk
			references cp_user
				on delete cascade,
	role_id bigint not null
		constraint cp_r_user_role_cp_d_role_id_fk
			references cp_d_role
				on delete cascade,
	constraint cp_r_user_role_pk
		primary key (user_id, role_id)
);


-- Add test users
insert into cp_user (email, password_hash, created_at, updated_at)
values (
    'test@test.com',
    '{bcrypt}$2a$10$zfrJDoHueDX1eCTA3clCh.Saq/3EnWpRi7KoHYlrSkCVqrK2FI2gK',
    '2020-10-03 10:33:01.712000',
    '2020-10-03 10:33:01.712000'
);
insert into cp_user (email, password_hash, created_at, updated_at)
values (
    'test2@test.com',
    '{bcrypt}$2a$10$r3lKf5hHHITs.lhGYA0As.Nhiv8Bd6ACOkdp4j5j.zComtalw3a5C',
    '2020-10-03 10:33:21.340000',
    '2020-10-03 10:33:21.340000'
);
insert into cp_r_user_role (user_id, role_id) VALUES (1, 1);
insert into cp_r_user_role (user_id, role_id) VALUES (1, 2);
insert into cp_r_user_role (user_id, role_id) VALUES (2, 1);