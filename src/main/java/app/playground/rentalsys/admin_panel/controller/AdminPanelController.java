package app.playground.rentalsys.admin_panel.controller;

import app.playground.rentalsys.user.entity.RoleCode;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Secured(RoleCode.ROLE_ADMIN_CODE)
@RequestMapping("/admin_panel")
public class AdminPanelController {

    @GetMapping("")
    public String getMainPage() {
        return "admin_panel/main";
    }

}
