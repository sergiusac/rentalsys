package app.playground.rentalsys.admin_panel.controller;

import app.playground.rentalsys.company.exception.RegisterCompanyException;
import app.playground.rentalsys.request.entity.RequestJournal;
import app.playground.rentalsys.request.exception.ChangeRequestStatusException;
import app.playground.rentalsys.request.service.RequestService;
import app.playground.rentalsys.user.entity.RoleCode;
import app.playground.rentalsys.util.Helper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@Secured(RoleCode.ROLE_ADMIN_CODE)
@RequestMapping("/admin_panel/income_requests")
public class IncomeRequestController {

    private final RequestService requestService;

    public IncomeRequestController(
            @Qualifier("defaultRequestService") RequestService requestService
    ) {
        this.requestService = requestService;
    }

    @GetMapping("")
    public String getRequestJournalPage(Model model) {
        List<RequestJournal> journals = requestService.getAllRequestJournals();
        model.addAttribute("journals", journals);
        return "admin_panel/income_requests/journal";
    }

    @GetMapping("/{id}")
    public String getRequestViewPage(Model model, @PathVariable("id") Long requestId) {
        RequestJournal request = requestService.getRequestJournalById(requestId);
        model.addAttribute("request", request);
        return "admin_panel/income_requests/request_view";
    }

    @PostMapping("/{id}/set_as_pending")
    public String setRequestAsPending(Model model, @PathVariable("id") Long requestId) {
        try {
            RequestJournal updatedRequest = requestService.setRequestAsPending(requestService.getRequestJournalById(requestId));
            model.addAttribute("request", updatedRequest);
            Helper.flashMessage(model, Helper.SUCCESS_MESSAGE, "Request status has been set as " + updatedRequest.getStatus().getName());
        } catch (ChangeRequestStatusException e) {
            Helper.flashMessage(model, Helper.ERROR_MESSAGE, e.getMessage());
        }
        return "admin_panel/income_requests/request_view";
    }

    @PostMapping("/{id}/accept")
    public String acceptRequest(Model model, @PathVariable("id") Long requestId) {
        try {
            RequestJournal updatedRequest = requestService.acceptRequest(requestService.getRequestJournalById(requestId));
            model.addAttribute("request", updatedRequest);
            Helper.flashMessage(model, Helper.SUCCESS_MESSAGE, "Request status has been set as " + updatedRequest.getStatus().getName());
        } catch (ChangeRequestStatusException | RegisterCompanyException e) {
            Helper.flashMessage(model, Helper.ERROR_MESSAGE, e.getMessage());
        }
        return "admin_panel/income_requests/request_view";
    }

    @PostMapping("/{id}/reject")
    public String rejectRequest(Model model, @PathVariable("id") Long requestId) {
        try {
            RequestJournal updatedRequest = requestService.rejectRequest(requestService.getRequestJournalById(requestId));
            model.addAttribute("request", updatedRequest);
            Helper.flashMessage(model, Helper.SUCCESS_MESSAGE, "Request status has been set as " + updatedRequest.getStatus().getName());
        } catch (ChangeRequestStatusException e) {
            Helper.flashMessage(model, Helper.ERROR_MESSAGE, e.getMessage());
        }
        return "admin_panel/income_requests/request_view";
    }
}
