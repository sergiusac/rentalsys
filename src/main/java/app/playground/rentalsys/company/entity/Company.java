package app.playground.rentalsys.company.entity;

import app.playground.rentalsys.common.entity.AbstractAuditingEntity;
import app.playground.rentalsys.request.entity.CompanyRegistrationRequest;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "cp_company")
public class Company extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "company_name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @OneToOne(optional = false)
    @JoinColumn(name = "registration_request_id", referencedColumnName = "id", nullable = false)
    private CompanyRegistrationRequest registrationRequest;

}
