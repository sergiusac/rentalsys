package app.playground.rentalsys.company.repo;

import app.playground.rentalsys.company.entity.Company;
import app.playground.rentalsys.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepo extends JpaRepository<Company, Long> {

    @Query("select c from Company c where c.registrationRequest.requestJournal.applicant = :user")
    List<Company> findAllUserCompanies(User user);
}
