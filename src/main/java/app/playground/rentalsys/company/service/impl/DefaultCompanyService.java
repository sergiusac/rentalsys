package app.playground.rentalsys.company.service.impl;

import app.playground.rentalsys.company.entity.Company;
import app.playground.rentalsys.company.exception.RegisterCompanyException;
import app.playground.rentalsys.company.repo.CompanyRepo;
import app.playground.rentalsys.company.service.CompanyService;
import app.playground.rentalsys.request.entity.CompanyRegistrationRequest;
import app.playground.rentalsys.request.service.CompanyRegistrationRequestService;
import app.playground.rentalsys.user.entity.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("defaultCompanyService")
public class DefaultCompanyService implements CompanyService {

    private final CompanyRegistrationRequestService companyRegistrationRequestService;
    private final CompanyRepo companyRepo;

    public DefaultCompanyService(
            @Qualifier("defaultCompanyRegistrationRequestService") CompanyRegistrationRequestService companyRegistrationRequestService,
            CompanyRepo companyRepo
    ) {
        this.companyRegistrationRequestService = companyRegistrationRequestService;
        this.companyRepo = companyRepo;
    }


    @Override
    public Company registerCompanyFromRequest(CompanyRegistrationRequest request) throws RegisterCompanyException {
        if (!companyRegistrationRequestService.isRequestAccepted(request)) {
            throw new RegisterCompanyException("Registration request is not accepted yet");
        }

        String companyName = request.getCompanyName();
        String companyInformation = request.getCompanyInformation();

        Company newCompany = new Company();
        newCompany.setName(companyName);
        newCompany.setDescription(companyInformation);
        newCompany.setRegistrationRequest(request);

        return companyRepo.save(newCompany);
    }

    @Override
    public User getCompanyRequestApplicant(Company company) {
        return company.getRegistrationRequest().getRequestJournal().getApplicant();
    }

    @Override
    public User getCompanyOwner(Company company) {
        return getCompanyRequestApplicant(company);
    }

    @Override
    public List<Company> getUserCompanies(User user) {
        return companyRepo.findAllUserCompanies(user);
    }

    @Override
    public Optional<Company> getCompanyById(long companyId) {
        return companyRepo.findById(companyId);
    }
}
