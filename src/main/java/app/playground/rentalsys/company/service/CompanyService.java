package app.playground.rentalsys.company.service;

import app.playground.rentalsys.company.entity.Company;
import app.playground.rentalsys.company.exception.RegisterCompanyException;
import app.playground.rentalsys.request.entity.CompanyRegistrationRequest;
import app.playground.rentalsys.request.entity.RequestJournal;
import app.playground.rentalsys.user.entity.User;

import java.util.List;
import java.util.Optional;

public interface CompanyService {
    Company registerCompanyFromRequest(CompanyRegistrationRequest journal) throws RegisterCompanyException;
    User getCompanyRequestApplicant(Company company);
    User getCompanyOwner(Company company);
    List<Company> getUserCompanies(User user);
    Optional<Company> getCompanyById(long companyId);
}
