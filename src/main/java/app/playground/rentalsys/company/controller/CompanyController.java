package app.playground.rentalsys.company.controller;

import app.playground.rentalsys.common.exception.RentalSysNotFoundException;
import app.playground.rentalsys.company.entity.Company;
import app.playground.rentalsys.company.service.CompanyService;
import app.playground.rentalsys.user.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping("/companies")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(
            @Qualifier("defaultCompanyService") CompanyService companyService
    ) {
        this.companyService = companyService;
    }

    @GetMapping("/{id}")
    public String getCompanyInfo(Model model, @PathVariable("id") long id) {
        Optional<Company> company = companyService.getCompanyById(id);

        if (company.isEmpty()) {
            throw new RentalSysNotFoundException("Company not found");
        }

        model.addAttribute("company", company.get());

        return "user/companies/company_info";
    }
}
