package app.playground.rentalsys.company.exception;

public class RegisterCompanyException extends Exception {

    public RegisterCompanyException() {
        super();
    }

    public RegisterCompanyException(String message) {
        super(message);
    }

    public RegisterCompanyException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegisterCompanyException(Throwable cause) {
        super(cause);
    }

    protected RegisterCompanyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
