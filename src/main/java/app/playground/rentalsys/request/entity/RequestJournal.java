package app.playground.rentalsys.request.entity;

import app.playground.rentalsys.common.entity.AbstractAuditingEntity;
import app.playground.rentalsys.user.entity.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "cp_request_journal")
public class RequestJournal extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ManyToOne(optional = false)
    @JoinColumn(name = "applicant_id", nullable = false)
    private User applicant;

    @Column(name = "remark")
    private String remark;

    @ManyToOne(optional = false)
    @JoinColumn(name = "request_type_id", nullable = false)
    private RequestType requestType;

    @ManyToOne(optional = false)
    @JoinColumn(name = "status_id", nullable = false)
    private RequestStatus status;

    @OneToOne(mappedBy = "requestJournal")
    private CompanyRegistrationRequest companyRegistrationRequest;

}
