package app.playground.rentalsys.request.entity;

import app.playground.rentalsys.common.entity.AbstractDictionaryEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "cp_d_request_type")
public class RequestType extends AbstractDictionaryEntity implements Serializable {
    private static final long serialVersionUID = 1L;
}
