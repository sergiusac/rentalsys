package app.playground.rentalsys.request.entity;

public class RequestStatusCode {
    public static final String SENT_CODE = "SENT";
    public static final String PENDING_CODE = "PENDING";
    public static final String ACCEPTED_CODE = "ACCEPTED";
    public static final String REJECTED_CODE = "REJECTED";
}
