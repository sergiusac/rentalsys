package app.playground.rentalsys.request.entity;

import app.playground.rentalsys.common.entity.AbstractAuditingEntity;
import app.playground.rentalsys.company.entity.Company;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "cp_company_registration_request")
public class CompanyRegistrationRequest extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "company_name", nullable = false)
    private String companyName;

    @Column(name = "company_info", nullable = false)
    private String companyInformation;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "journal_id", referencedColumnName = "id",nullable = false)
    private RequestJournal requestJournal;

    @OneToOne(mappedBy = "registrationRequest")
    private Company company;

}
