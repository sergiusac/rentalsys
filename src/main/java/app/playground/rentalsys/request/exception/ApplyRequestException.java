package app.playground.rentalsys.request.exception;

public class ApplyRequestException extends Exception {

    public ApplyRequestException() {
        super();
    }

    public ApplyRequestException(String message) {
        super(message);
    }

    public ApplyRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplyRequestException(Throwable cause) {
        super(cause);
    }

    protected ApplyRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
