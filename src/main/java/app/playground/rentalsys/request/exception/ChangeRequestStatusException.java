package app.playground.rentalsys.request.exception;

public class ChangeRequestStatusException extends Exception {

    public ChangeRequestStatusException() {
        super();
    }

    public ChangeRequestStatusException(String message) {
        super(message);
    }

    public ChangeRequestStatusException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChangeRequestStatusException(Throwable cause) {
        super(cause);
    }

    protected ChangeRequestStatusException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
