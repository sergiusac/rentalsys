package app.playground.rentalsys.request.data;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class CompanyRequestData implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    @NotEmpty
    private String companyName;

    @NotNull
    @NotEmpty
    private String companyInformation;

    private String remark;
}
