package app.playground.rentalsys.request.controller;

import app.playground.rentalsys.request.data.CompanyRequestData;
import app.playground.rentalsys.request.entity.CompanyRegistrationRequest;
import app.playground.rentalsys.request.entity.RequestJournal;
import app.playground.rentalsys.request.exception.ApplyRequestException;
import app.playground.rentalsys.request.service.CompanyRegistrationRequestService;
import app.playground.rentalsys.request.service.RequestService;
import app.playground.rentalsys.user.entity.User;
import app.playground.rentalsys.user.service.UserService;
import app.playground.rentalsys.util.Helper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/requests")
public class RequestController {

    private final UserService userService;
    private final RequestService requestService;
    private final CompanyRegistrationRequestService companyRegistrationRequestService;

    public RequestController(
            @Qualifier("defaultUserService") UserService userService,
            @Qualifier("defaultRequestService") RequestService requestService,
            @Qualifier("defaultCompanyRegistrationRequestService") CompanyRegistrationRequestService companyRegistrationRequestService) {
        this.userService = userService;
        this.requestService = requestService;
        this.companyRegistrationRequestService = companyRegistrationRequestService;
    }

    @GetMapping("/company_registration")
    public String getCompanyRegistrationPage(Model model) {
        CompanyRequestData companyRequestData = new CompanyRequestData();
        model.addAttribute("company", companyRequestData);
        return "user/requests/company_registration";
    }

    @PostMapping("/company_registration")
    public String registerCompany(Principal principal, Model model, @Valid @ModelAttribute("company") CompanyRequestData company, BindingResult result) {
        Optional<User> user = userService.getUserFromPrincipal(principal);
        if (user.isPresent()) {
            try {
                CompanyRegistrationRequest appliedRequest = companyRegistrationRequestService.applyCompanyRegistrationRequest(user.get(), company);
                Helper.flashMessage(model, Helper.SUCCESS_MESSAGE, "Your request has been applied. Its ID is " + appliedRequest.getRequestJournal().getId());
            } catch (ApplyRequestException e) {
                Helper.flashMessage(model, Helper.ERROR_MESSAGE, e.getMessage());
            }
        }
        return "user/requests/company_registration";
    }

    @GetMapping("/history")
    public String getHistory(Principal principal, Model model) {
        Optional<User> user = userService.getUserFromPrincipal(principal);
        if (user.isPresent()) {
            List<RequestJournal> journals = requestService.getRequestJournalsByApplicant(user.get());
            model.addAttribute("journals", journals);
        }
        return "user/requests/history";
    }
}
