package app.playground.rentalsys.request.repo;

import app.playground.rentalsys.request.entity.RequestJournal;
import app.playground.rentalsys.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestJournalRepo extends JpaRepository<RequestJournal, Long> {
    List<RequestJournal> findByApplicantOrderByCreatedAtDesc(User applicant);
}
