package app.playground.rentalsys.request.repo;

import app.playground.rentalsys.request.entity.CompanyRegistrationRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRegistrationRequestRepo extends JpaRepository<CompanyRegistrationRequest, Long> {
}
