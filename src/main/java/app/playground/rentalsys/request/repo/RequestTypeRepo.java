package app.playground.rentalsys.request.repo;

import app.playground.rentalsys.request.entity.RequestType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestTypeRepo extends JpaRepository<RequestType, Long> {
    RequestType findByCode(String code);
}
