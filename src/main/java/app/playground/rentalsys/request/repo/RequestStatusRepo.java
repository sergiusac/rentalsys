package app.playground.rentalsys.request.repo;

import app.playground.rentalsys.request.entity.RequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestStatusRepo extends JpaRepository<RequestStatus, Long> {
    RequestStatus findByCode(String code);
}
