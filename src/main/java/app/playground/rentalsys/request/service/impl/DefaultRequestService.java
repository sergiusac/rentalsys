package app.playground.rentalsys.request.service.impl;

import app.playground.rentalsys.common.exception.RentalSysNotFoundException;
import app.playground.rentalsys.company.exception.RegisterCompanyException;
import app.playground.rentalsys.company.service.CompanyService;
import app.playground.rentalsys.request.data.CompanyRequestData;
import app.playground.rentalsys.request.entity.*;
import app.playground.rentalsys.request.exception.ApplyRequestException;
import app.playground.rentalsys.request.exception.ChangeRequestStatusException;
import app.playground.rentalsys.request.repo.CompanyRegistrationRequestRepo;
import app.playground.rentalsys.request.repo.RequestJournalRepo;
import app.playground.rentalsys.request.repo.RequestStatusRepo;
import app.playground.rentalsys.request.repo.RequestTypeRepo;
import app.playground.rentalsys.request.service.RequestService;
import app.playground.rentalsys.user.entity.RoleCode;
import app.playground.rentalsys.user.entity.User;
import app.playground.rentalsys.user.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service("defaultRequestService")
public class DefaultRequestService implements RequestService {

    private final UserService userService;
    private final CompanyService CompanyService;
    private final CompanyRegistrationRequestRepo companyRegistrationRequestRepo;
    private final RequestTypeRepo requestTypeRepo;
    private final RequestJournalRepo requestJournalRepo;
    private final RequestStatusRepo requestStatusRepo;

    public DefaultRequestService(
            @Qualifier("defaultUserService") UserService userService,
            @Qualifier("defaultCompanyService") CompanyService CompanyService,
            CompanyRegistrationRequestRepo companyRegistrationRequestRepo,
            RequestTypeRepo requestTypeRepo, RequestJournalRepo requestJournalRepo,
            RequestStatusRepo requestStatusRepo
    ) {
        this.userService = userService;
        this.CompanyService = CompanyService;
        this.companyRegistrationRequestRepo = companyRegistrationRequestRepo;
        this.requestTypeRepo = requestTypeRepo;
        this.requestJournalRepo = requestJournalRepo;
        this.requestStatusRepo = requestStatusRepo;
    }

    @Override
    public List<RequestJournal> getRequestJournalsByApplicant(User applicant) {
        return requestJournalRepo.findByApplicantOrderByCreatedAtDesc(applicant);
    }

    @Override
    public List<RequestJournal> getAllRequestJournals() {
        return requestJournalRepo.findAll(Sort.by("createdAt").descending());
    }

    @Override
    public RequestJournal getRequestJournalById(Long requestJournalId) {
        Optional<RequestJournal> journal = requestJournalRepo.findById(requestJournalId);
        if (journal.isEmpty()) {
            throw new RentalSysNotFoundException("Request Not Found");
        }
        return journal.get();
    }

    @Override
    public RequestJournal acceptRequest(RequestJournal requestJournal) throws ChangeRequestStatusException, RegisterCompanyException {
        RequestStatus status = requestStatusRepo.findByCode(RequestStatusCode.ACCEPTED_CODE);

        if (status == null) {
            throw new ChangeRequestStatusException("Unknown status '" + RequestStatusCode.ACCEPTED_CODE + "'");
        }

        requestJournal.setStatus(status);
        RequestJournal acceptedRequest = requestJournalRepo.save(requestJournal);

        // if it is the request for registering a new company
        if (acceptedRequest.getCompanyRegistrationRequest() != null) {
            // automatically create new company after accepting the company registration request
            CompanyService.registerCompanyFromRequest(acceptedRequest.getCompanyRegistrationRequest());
        }

        return acceptedRequest;
    }

    @Override
    public RequestJournal setRequestAsPending(RequestJournal requestJournal) throws ChangeRequestStatusException {
        RequestStatus status = requestStatusRepo.findByCode(RequestStatusCode.PENDING_CODE);
        if (status == null) {
            throw new ChangeRequestStatusException("Unknown status '" + RequestStatusCode.PENDING_CODE + "'");
        }
        requestJournal.setStatus(status);
        return requestJournalRepo.save(requestJournal);
    }

    @Override
    public RequestJournal rejectRequest(RequestJournal requestJournal) throws ChangeRequestStatusException {
        RequestStatus status = requestStatusRepo.findByCode(RequestStatusCode.REJECTED_CODE);
        if (status == null) {
            throw new ChangeRequestStatusException("Unknown status '" + RequestStatusCode.REJECTED_CODE + "'");
        }
        requestJournal.setStatus(status);
        return requestJournalRepo.save(requestJournal);
    }
}
