package app.playground.rentalsys.request.service;

import app.playground.rentalsys.company.exception.RegisterCompanyException;
import app.playground.rentalsys.request.entity.RequestJournal;
import app.playground.rentalsys.request.exception.ChangeRequestStatusException;
import app.playground.rentalsys.user.entity.User;

import java.util.List;

public interface RequestService {
    List<RequestJournal> getRequestJournalsByApplicant(User applicant);
    List<RequestJournal> getAllRequestJournals();
    RequestJournal getRequestJournalById(Long requestJournalId);
    RequestJournal acceptRequest(RequestJournal requestJournal) throws ChangeRequestStatusException, RegisterCompanyException;
    RequestJournal setRequestAsPending(RequestJournal requestJournal) throws ChangeRequestStatusException;
    RequestJournal rejectRequest(RequestJournal requestJournal) throws ChangeRequestStatusException;
}
