package app.playground.rentalsys.request.service;

import app.playground.rentalsys.request.data.CompanyRequestData;
import app.playground.rentalsys.request.entity.CompanyRegistrationRequest;
import app.playground.rentalsys.request.exception.ApplyRequestException;
import app.playground.rentalsys.user.entity.User;

public interface CompanyRegistrationRequestService {
    CompanyRegistrationRequest applyCompanyRegistrationRequest(User applicant, CompanyRequestData companyRequestData) throws ApplyRequestException;
    boolean isRequestAccepted(CompanyRegistrationRequest request);
}
