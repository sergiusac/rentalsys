package app.playground.rentalsys.request.service.impl;

import app.playground.rentalsys.request.data.CompanyRequestData;
import app.playground.rentalsys.request.entity.*;
import app.playground.rentalsys.request.exception.ApplyRequestException;
import app.playground.rentalsys.request.repo.CompanyRegistrationRequestRepo;
import app.playground.rentalsys.request.repo.RequestJournalRepo;
import app.playground.rentalsys.request.repo.RequestStatusRepo;
import app.playground.rentalsys.request.repo.RequestTypeRepo;
import app.playground.rentalsys.request.service.CompanyRegistrationRequestService;
import app.playground.rentalsys.user.entity.RoleCode;
import app.playground.rentalsys.user.entity.User;
import app.playground.rentalsys.user.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("defaultCompanyRegistrationRequestService")
public class DefaultCompanyRegistrationRequestService implements CompanyRegistrationRequestService {

    private final UserService userService;
    private final CompanyRegistrationRequestRepo companyRegistrationRequestRepo;
    private final RequestTypeRepo requestTypeRepo;
    private final RequestStatusRepo requestStatusRepo;

    public DefaultCompanyRegistrationRequestService(
            @Qualifier("defaultUserService") UserService userService,
            CompanyRegistrationRequestRepo companyRegistrationRequestRepo,
            RequestTypeRepo requestTypeRepo,
            RequestStatusRepo requestStatusRepo
    ) {
        this.userService = userService;
        this.companyRegistrationRequestRepo = companyRegistrationRequestRepo;
        this.requestTypeRepo = requestTypeRepo;
        this.requestStatusRepo = requestStatusRepo;
    }

    @Transactional
    @Override
    public CompanyRegistrationRequest applyCompanyRegistrationRequest(User applicant, CompanyRequestData companyRequestData) throws ApplyRequestException {
        if (userService.hasRole(applicant, RoleCode.ROLE_ADMIN_CODE)) {
            throw new ApplyRequestException("You cannot apply this type of request");
        }

        RequestType type = requestTypeRepo.findByCode(RequestTypeCode.COMPANY_REGISTRATION_CODE);
        RequestStatus statusSent = requestStatusRepo.findByCode(RequestStatusCode.SENT_CODE);

        RequestJournal journal = new RequestJournal();
        journal.setApplicant(applicant);
        journal.setRequestType(type);
        journal.setStatus(statusSent);
        if (companyRequestData.getRemark() != null && !companyRequestData.getRemark().isEmpty()) {
            journal.setRemark(companyRequestData.getRemark());
        }

        CompanyRegistrationRequest request = new CompanyRegistrationRequest();
        request.setCompanyName(companyRequestData.getCompanyName());
        request.setCompanyInformation(companyRequestData.getCompanyInformation());
        request.setRequestJournal(journal);

        return companyRegistrationRequestRepo.save(request);
    }

    @Override
    public boolean isRequestAccepted(CompanyRegistrationRequest request) {
        return request.getRequestJournal().getStatus().getCode().equals(RequestStatusCode.ACCEPTED_CODE);
    }
}
