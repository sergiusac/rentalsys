package app.playground.rentalsys.util;

import org.springframework.ui.Model;

import java.util.Collections;

public class Helper {

    public static final String ERROR_MESSAGE = "ERROR";
    public static final String WARING_MESSAGE = "WARNING";
    public static final String INFO_MESSAGE = "INFO";
    public static final String SUCCESS_MESSAGE = "SUCCESS";

    public static void flashMessage(Model model, String type, String msg) {
        switch (type) {
            case ERROR_MESSAGE: {
                model.addAttribute("globalErrorMessages", Collections.singleton(msg));
                break;
            }
            case WARING_MESSAGE: {
                model.addAttribute("globalWarningMessages", Collections.singleton(msg));
                break;
            }
            case INFO_MESSAGE: {
                model.addAttribute("globalInfoMessages", Collections.singleton(msg));
                break;
            }
            default: {
                model.addAttribute("globalSuccessMessages", Collections.singleton(msg));
                break;
            }
        }
    }
}
