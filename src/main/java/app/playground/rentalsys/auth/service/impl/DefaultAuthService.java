package app.playground.rentalsys.auth.service.impl;

import app.playground.rentalsys.auth.controller.request.UserSignUpRequest;
import app.playground.rentalsys.auth.exception.AuthException;
import app.playground.rentalsys.auth.service.AuthService;
import app.playground.rentalsys.user.entity.Role;
import app.playground.rentalsys.user.entity.User;
import app.playground.rentalsys.user.entity.UserRole;
import app.playground.rentalsys.user.repo.RoleRepo;
import app.playground.rentalsys.user.repo.UserRepo;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service("defaultAuthService")
public class DefaultAuthService implements AuthService {

    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final PasswordEncoder passwordEncoder;

    public DefaultAuthService(UserRepo userRepo, RoleRepo roleRepo, PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    @Override
    public User signUp(UserSignUpRequest userSignUpRequest) throws AuthException {
        Optional<User> user = userRepo.findByEmail(userSignUpRequest.getEmail());

        if (user.isPresent()) {
            throw new AuthException("The email is already used");
        }

        Role standardRole = roleRepo.findByCode("ROLE_USER");

        User newUser = new User();
        newUser.setEmail(userSignUpRequest.getEmail());
        newUser.setPasswordHash(passwordEncoder.encode(userSignUpRequest.getPassword()));

        UserRole userRole = new UserRole();
        userRole.setUser(newUser);
        userRole.setRole(standardRole);
        newUser.getUserRoles().add(userRole);

        return userRepo.save(newUser);
    }
}
