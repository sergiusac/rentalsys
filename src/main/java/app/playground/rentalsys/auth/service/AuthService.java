package app.playground.rentalsys.auth.service;

import app.playground.rentalsys.auth.controller.request.UserSignUpRequest;
import app.playground.rentalsys.auth.exception.AuthException;
import app.playground.rentalsys.user.entity.User;

public interface AuthService {
    User signUp(UserSignUpRequest userSignUpRequest) throws AuthException;
}
