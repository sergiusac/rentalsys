package app.playground.rentalsys.auth.controller;

import app.playground.rentalsys.auth.controller.request.UserSignUpRequest;
import app.playground.rentalsys.auth.exception.AuthException;
import app.playground.rentalsys.auth.service.AuthService;
import app.playground.rentalsys.user.entity.User;
import app.playground.rentalsys.util.Helper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;

@Slf4j
@Controller
@RequestMapping("")
public class AuthController {

    private final AuthService authService;

    public AuthController(@Qualifier("defaultAuthService") AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/auth/login")
    public String getLoginPage(Model model) {
        return "auth/login";
    }

    @GetMapping("/auth/signup")
    public String getSignUpPage(Model model) {
        UserSignUpRequest user = new UserSignUpRequest();
        model.addAttribute("user", user);
        return "auth/signup";
    }

    @PostMapping("/auth/signup")
    public String doSignUp(Model model, @Valid @ModelAttribute("user") UserSignUpRequest signUpRequest, BindingResult result) {
        if (result.hasErrors()) {
            return "auth/signup";
        }

        try {
            User newUser = authService.signUp(signUpRequest);
            model.addAttribute("user", newUser);
            return "redirect:/auth/login";
        } catch (AuthException e) {
            Helper.flashMessage(model, Helper.ERROR_MESSAGE, e.getMessage());
            return "auth/signup";
        }
    }
}
