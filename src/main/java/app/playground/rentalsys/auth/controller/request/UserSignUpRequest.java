package app.playground.rentalsys.auth.controller.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class UserSignUpRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    @NotBlank
    @Email(message = "Type correct email address")
    private String email;

    @NotNull
    @NotBlank
    @Size(min = 6, message = "Password must contain at least 6 characters")
    private String password;
}
