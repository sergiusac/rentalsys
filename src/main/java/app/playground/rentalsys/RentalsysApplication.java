package app.playground.rentalsys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentalsysApplication {

    public static void main(String[] args) {
        SpringApplication.run(RentalsysApplication.class, args);
    }

}
