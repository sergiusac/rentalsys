package app.playground.rentalsys.user.service.impl;

import app.playground.rentalsys.common.entity.AbstractDictionaryEntity;
import app.playground.rentalsys.user.entity.Role;
import app.playground.rentalsys.user.entity.User;
import app.playground.rentalsys.user.entity.UserRole;
import app.playground.rentalsys.user.repo.UserRepo;
import app.playground.rentalsys.user.service.UserService;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("defaultUserService")
public class DefaultUserService implements UserService {

    private final UserRepo userRepo;

    public DefaultUserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public boolean hasRole(User user, String roleCode) {
        List<String> roles = getRoleCodes(user);
        return roles.contains(roleCode);
    }

    @Override
    public List<Role> getRoles(User user) {
        return user.getUserRoles().stream().map(UserRole::getRole).collect(Collectors.toList());
    }

    @Override
    public List<String> getRoleCodes(User user) {
        return getRoles(user).stream().map(AbstractDictionaryEntity::getCode).collect(Collectors.toList());
    }

    @Override
    public Optional<User> getUserFromPrincipal(Principal principal) {
        return userRepo.findByEmail(principal.getName());
    }


}
