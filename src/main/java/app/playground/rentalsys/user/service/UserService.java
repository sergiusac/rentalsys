package app.playground.rentalsys.user.service;

import app.playground.rentalsys.user.entity.Role;
import app.playground.rentalsys.user.entity.User;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface UserService {
    boolean hasRole(User user, String roleCode);
    List<Role> getRoles(User user);
    List<String> getRoleCodes(User user);
    Optional<User> getUserFromPrincipal(Principal principal);
}
