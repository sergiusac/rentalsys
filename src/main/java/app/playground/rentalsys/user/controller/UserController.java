package app.playground.rentalsys.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/user")
public class UserController {

    @GetMapping("/cabinet")
    public String getUserHomePage(Principal principal, Model model) {
        model.addAttribute("user", principal);
        return "user/cabinet";
    }
}
