package app.playground.rentalsys.user.repo;

import app.playground.rentalsys.user.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepo extends JpaRepository<Role, Long> {
    Role findByCode(String code);
}
