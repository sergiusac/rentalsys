package app.playground.rentalsys.user.entity;

public class RoleCode {
    public static final String ROLE_USER_CODE = "ROLE_USER";
    public static final String ROLE_ADMIN_CODE = "ROLE_ADMIN";
}
