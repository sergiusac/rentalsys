package app.playground.rentalsys.user.entity;

import app.playground.rentalsys.common.entity.AbstractDictionaryEntity;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "cp_d_role")
public class Role extends AbstractDictionaryEntity implements GrantedAuthority, Serializable {
    private static final long serialVersionUID = 1L;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<UserRole> userRoles = new ArrayList<>();

    @Override
    public String getAuthority() {
        return getCode();
    }
}
