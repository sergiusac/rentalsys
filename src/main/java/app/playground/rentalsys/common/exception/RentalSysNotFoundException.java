package app.playground.rentalsys.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class RentalSysNotFoundException extends RuntimeException {

    public RentalSysNotFoundException() {
        super();
    }

    public RentalSysNotFoundException(String message) {
        super(message);
    }

    public RentalSysNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RentalSysNotFoundException(Throwable cause) {
        super(cause);
    }

    protected RentalSysNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
