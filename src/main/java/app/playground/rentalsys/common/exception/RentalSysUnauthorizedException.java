package app.playground.rentalsys.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class RentalSysUnauthorizedException extends RuntimeException {

    public RentalSysUnauthorizedException() {
        super();
    }

    public RentalSysUnauthorizedException(String message) {
        super(message);
    }

    public RentalSysUnauthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public RentalSysUnauthorizedException(Throwable cause) {
        super(cause);
    }

    protected RentalSysUnauthorizedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
